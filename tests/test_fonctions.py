import unittest
from fonctions import *

def crea_base_test():
    str_wr = {
        'name':'Star War',
        'prenom':{
            'no_lettre':1,
            'corresp':{
                'A':'Darth',
                'B':'Padamé',
                'C':'Jar Jar',
                'D':'Obi-Wan',
                'E':'Emperor',
                'F':'Ham',
                'G':'Greedo',
                'H':'Anakin',
                'I':'Count',
                'J':'Sarlacc',
                'K':'Jabba',
                'L':'Sergent',
                'M':'Boba',
                'N':'Wicket W',
                'O':'Wampa',
                'P':'Senator',
                'Q':'Queen',
                'R':'Padawan',
                'S':'Imperial',
                'T':'General',
                'U':'Storm',
                'V':'R2',
                'W':'Like',
                'X':'C3',
                'Y':'Mace',
                'Z':'Princess',
            },
        },
        'nom':{
            'no_lettre':3,
            'corresp':{
                'A':'Bane',
                'B':'Sidious',
                'C':'Skywalker',
                'D':'Chewbacca',
                'E':'Fett',
                'F':'Trooper',
                'G':'PO',
                'H':'Speeder',
                'I':'Dooku',
                'J':'Pit',
                'K':'Vader',
                'L':'D2',
                'M':'Palpatine',
                'N':'Grievous',
                'O':'Binks',
                'P':'Soldier',
                'Q':'Kenobi',
                'R':'Solo',
                'S':'The Hutt',
                'T':'Maul',
                'U':'Windu',
                'V':'Queen',
                'W':'Leia',
                'X':'Monster',
                'Y':'Yoda',
                'Z':'Warrick',
            },
        },
    }
    return str_wr

class TestFonctions(unittest.TestCase):
    def test_generer(self) :
        str_wr = crea_base_test()
        lst_dico = [str_wr,]
        self.assertEqual('Jar Jar Palpatine', generer(lst_dico, 'Star War', 'Christine', 'Tommal'))
        self.assertEqual('Base inexistante', generer(lst_dico, 'Le Hobbit', 'Hervé', 'Cloclo'))
        self.assertEqual('Entrée prénom invalide', generer(lst_dico, 'Star War', '', 'Tommal'))
        self.assertEqual('Entrée nom invalide', generer(lst_dico, 'Star War', 'Christine', 'To'))
    
    def test_creer(self):
        str_wr = crea_base_test()
        lst_dico = []
        lst_test = [str_wr,]
        creer_base(lst_dico, str_wr)
        self.assertEqual(lst_dico, lst_test)

    def test_verif_dico_valid(self):
        str_wr = crea_base_test()
        self.assertEqual(verif_dico_valid(str_wr), True)

        str_wr_inva = {
            'name':'Star War',
            'prenom':{
                'no_lettre':1,
                'corresp':{
                    'A':'Darth',
                    'B':'Padamé',
                    'C':'Jar Jar',
                    'D':'Obi-Wan',
                    'E':'Emperor',
                    'F':'Ham',
                    'G':'Greedo',
                    'H':'Anakin',
                    'I':'Count',
                    'J':'Sarlacc',
                    'K':'Jabba',
                    'L':'Sergent',
                    'M':'Boba',
                    'N':'Wicket W',
                    'O':'Wampa',
                    'P':'Senator',
                    'Q':'Queen',
                    'R':'Padawan',
                    'S':'Imperial',
                    'T':'General',
                    'U':'Storm',
                    'V':'R2',
                    'W':'Like',
                    'X':'C3',
                    'Y':'Mace',
                    'Z':'Princess',
                },
            },
        }
        self.assertEqual(verif_dico_valid(str_wr_inva), False)

    def test_verif_dico_exist(self):
        str_wr = crea_base_test()
        lst_dico = [str_wr,]
        self.assertEqual(verif_dico_exist(lst_dico, 'Star War'), str_wr)
        self.assertEqual(verif_dico_exist(lst_dico, 'Game of Thrones'), False)