def creer_base(liste_dico, dictionnaire) :
    liste_dico.append(dictionnaire)

def verif_dico_valid(dictionnaire) :
    if('name' not in dictionnaire):
        return False
    if('prenom' not in dictionnaire) :
        return False
    if('nom' not in dictionnaire):
        return False
    return True

def generer(liste_dico, dico_nom, prenom, nom) :
    dico = verif_dico_exist(liste_dico, dico_nom)
    if(dico==False):
        return 'Base inexistante'
    if(len(prenom)<dico['prenom']['no_lettre']):
        return 'Entrée prénom invalide'
    if(len(nom)<dico['nom']['no_lettre']):
        return 'Entrée nom invalide'
    

def verif_dico_exist(liste_dico, dico_nom) :
    raise NotImplementedError